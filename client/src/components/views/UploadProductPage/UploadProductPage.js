import React, { useState } from 'react';
import { Button, Form, Input, Typography } from 'antd';
import FileUpload from '../../utils/FileUpload';
import Axios from 'axios';

const { Title } = Typography;
const { TextArea } = Input;

const Continents = [
  { key: 1, value: 'Africa' },
  { key: 2, value: 'Europe' },
  { key: 3, value: 'Asia' },
  { key: 4, value: 'North America' },
  { key: 5, value: 'South America' },
  { key: 6, value: 'Australia' },
  { key: 7, value: 'Antarctica' },
];

function UploadProductPage(props) {
  const [TitleValue, setTitleValue] = useState('');
  const [DescriptionValue, setDescriptionValue] = useState('');
  const [PriceValue, setPriceValue] = useState(0);
  const [ContinentsValue, setContinentsValue] = useState(1);
  const [Images, setImages] = useState([]);

  const [ErrorsValue, setErrorsValue] = useState({});

  const onTitleChange = (event) => {
    setTitleValue(event.currentTarget.value);
  };

  const onDescriptionChange = (event) => {
    setDescriptionValue(event.currentTarget.value);
  };

  const onPriceChange = (event) => {
    setPriceValue(event.currentTarget.value);
  };

  const onContinentsSelectChange = (event) => {
    setContinentsValue(event.currentTarget.value);
  };

  const updateImages = (newImages) => {
    console.log('update images fired from Upload product page');
    setImages(newImages);
  };

  const handleValidation = () => {
    let errors = {};
    let formIsValid = true;

    if (!Images) {
      formIsValid = false;
      errors['images'] = "Title field can't be empty";
    }

    if (!TitleValue) {
      formIsValid = false;
      errors['title'] = "Title field can't be empty";
    }

    if (!DescriptionValue) {
      formIsValid = false;
      errors['description'] = "Description field can't be empty";
    }

    setErrorsValue(errors);
    return formIsValid;
  };

  const onSubmit = (event) => {
    event.preventDefault();

    if (handleValidation()) {
      const options = {
        writer: props.user.userData._id,
        title: TitleValue,
        description: DescriptionValue,
        price: PriceValue,
        images: Images,
        continents: ContinentsValue,
      };
      Axios.post('/api/product/uploadProduct', options).then((response) => {
        if (response.data.success) {
          alert('Product Successfully Uploaded');
          props.history.push('/');
        } else {
          alert('Failed to upload Product');
        }
      });
    }
  };

  return (
    <div style={{ maxWidth: '700px', margin: '6rem auto' }}>
      <div style={{ textAlign: 'center', marginBottom: '2rem' }}>
        <Title level={2}> Upload Product Image </Title>
      </div>

      <Form onSubmit={onSubmit}>
        <FileUpload refreshFunction={updateImages} />

        <br />
        <br />
        <label>Title</label>
        <Input onChange={onTitleChange} value={TitleValue} />
        <span style={{ color: 'red' }}>{ErrorsValue['title']}</span>
        <br />
        <br />
        <label>Description</label>
        <TextArea onChange={onDescriptionChange} value={DescriptionValue} />
        <span style={{ color: 'red' }}>{ErrorsValue['description']}</span>
        <br />
        <br />
        <label>Price($)</label>
        <Input onChange={onPriceChange} value={PriceValue} type="number" />
        <br />
        <br />

        <label>Continents: </label>
        <br />
        <select onChange={onContinentsSelectChange}>
          {Continents.map((type) => (
            <option key={type.key} value={type.key}>
              {type.value}
            </option>
          ))}
        </select>
        <br />
        <br />
        <Button onClick={onSubmit}>Submit</Button>
      </Form>
    </div>
  );
}

export default UploadProductPage;
