import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { Icon, Row, Col, Card, Button, Typography } from 'antd';
import ImageSlider from '../../utils/ImageSlider';
import CheckBox from './Sections/CheckBox';
import RadioBox from './Sections/RadioBox';
import SearchFeature from './Sections/SearchFeature';

const { Title } = Typography;
const { Meta } = Card;

function LandingPage() {
  const [Products, setProducts] = useState([]);
  const [Skip, setSkip] = useState(0);
  const [Limit, setLimit] = useState(6);
  const [PostSize, setPostSize] = useState(0);
  const [SearchTerms, setSearchTerms] = useState('');
  const [Filters, setFilters] = useState({
    continents: [],
    price: 0,
  });

  useEffect(() => {
    const options = {
      skip: Skip,
      limit: Limit,
      filters: Filters,
    };
    getProducts(options);
  }, []);

  const getProducts = (options) => {
    Axios.post('api/product/getProducts', options).then((response) => {
      if (response.data.success) {
        setProducts(response.data.products);
        setPostSize(response.data.postSize);
      } else {
        alert('Failed to fetch product data');
      }
    });
  };

  const onMove = (direction) => {
    let skip = Skip + Limit * direction;
    if (skip < 0) skip = 0;
    const options = {
      skip: skip,
      limit: Limit,
      filters: Filters,
    };
    getProducts(options);
    setSkip(skip);
  };

  const RenderCards = Products.map((product, index) => {
    return (
      <Col lg={8} md={12} xs={24}>
        <Card
          hoverable={true}
          cover={
            <a href={`/product/${product._id}`}>
              <ImageSlider images={product.images} />
            </a>
          }
        >
          <Meta title={product.title} description={`$${product.price}`} />
        </Card>
      </Col>
    );
  });

  const showFilteredResults = (filters) => {
    const options = {
      skip: 0,
      limit: Limit,
      filters,
    };
    getProducts(options);
    setSkip(0);
  };

  const handleFilters = (filters, category) => {
    console.log(filters);
    const newFilters = { ...Filters };

    newFilters[category] = filters;

    showFilteredResults(newFilters);

    setFilters(newFilters);
  };

  const updateSearchTerms = (newSearchTerm) => {
    console.log(newSearchTerm);

    const options = {
      skip: 0,
      limit: Limit,
      filters: Filters,
      searchTerm: newSearchTerm,
    };

    setSkip(0);
    setSearchTerms(newSearchTerm);
    getProducts(options);
  };

  return (
    <div style={{ width: '75%', margin: '3rem auto' }}>
      <div style={{ textAlign: 'center' }}>
        <h2>
          {' '}
          Let's Travel Anywhere <Icon type="rocket" />
        </h2>
      </div>

      <br />
      {/* Filter */}

      <Row gutter={[16, 16]}>
        <Col lg={12} xs={24}>
          <CheckBox
            handleFilters={(filters) => handleFilters(filters, 'continents')}
          />
        </Col>
        <Col lg={12} xs={24}>
          <RadioBox
            handleFilters={(filters) => handleFilters(filters, 'price')}
          />
        </Col>
      </Row>

      {/* Search */}
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          margin: '1rem auto',
        }}
      >
        <SearchFeature refreshFunction={updateSearchTerms} />
      </div>

      {Products.length === 0 ? (
        <div
          style={{
            display: 'flex',
            height: '300px',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Title level={2}>No posts yet...</Title>
        </div>
      ) : (
        <div>
          <Row gutter={[16, 16]}>{RenderCards}</Row>
        </div>
      )}
      <br />
      <br />
      <div
        style={{ display: 'flex', justifyContent: 'center', padding: '0 5px' }}
      >
        {Skip > 0 && (
          <div style={{ margin: '0 5px' }}>
            <Button onClick={() => onMove(-1)}>Prev</Button>
          </div>
        )}
        {PostSize >= Limit && (
          <div style={{ margin: '0 5px' }}>
            <Button onClick={() => onMove(1)}>Next</Button>
          </div>
        )}
      </div>
    </div>
  );
}

export default LandingPage;
