import React, { useState } from 'react';
import { Collapse, Radio } from 'antd';

const { Panel } = Collapse;

const price = [
  { name: 'lowest first', sort: 1 },
  { name: 'highest first', sort: -1 },
];

function RadioBox(props) {
  const [Value, setValue] = useState(undefined);

  const renderRadioBox = price.map((value, index) => (
    <Radio key={index} value={value.sort}>
      {value.name}
    </Radio>
  ));

  const handleChange = (event) => {
    setValue(event.target.value);
    props.handleFilters(event.target.value);
  };

  return (
    <div>
      <Collapse defaultActiveKey={['0']}>
        <Panel header="Sort by Price" key="1">
          <Radio.Group onChange={handleChange} value={Value}>
            {renderRadioBox}
          </Radio.Group>
        </Panel>
      </Collapse>
    </div>
  );
}

export default RadioBox;
